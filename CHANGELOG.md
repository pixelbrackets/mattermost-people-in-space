# Changelog

2020-09-26 Dan Untenzu <mail@pixelbrackets.de>

  * 1.4.0
  * FEATURE Use API wrapper package
  * FEATURE Set URL in help page via env variable

2020-09-25 Dan Untenzu <mail@pixelbrackets.de>

  * 1.3.0
  * FEATURE Docs: Change demo URL

2020-04-20 Dan Untenzu <mail@pixelbrackets.de>

  * 1.2.0
  * FEATURE Style help page
  * FEATURE Add CI

2020-04-03 Dan Untenzu <mail@pixelbrackets.de>

  * 1.1.0
  * FEATURE Get latest data via API

2020-04-03 Dan Untenzu <mail@pixelbrackets.de>

  * 1.0.0
  * FEATURE Inital version
