# Mattermost People in Space Integration

[![Build Status](https://img.shields.io/gitlab/pipeline/pixelbrackets/mattermost-people-in-space?style=flat-square)](https://gitlab.com/pixelbrackets/mattermost-people-in-space/pipelines)
[![Made With](https://img.shields.io/badge/made_with-php-blue?style=flat-square)](https://gitlab.com/pixelbrackets/mattermost-people-in-space#requirements)
[![License](https://img.shields.io/badge/license-gpl--2.0--or--later-blue.svg?style=flat-square)](https://spdx.org/licenses/GPL-2.0-or-later.html)

This integration shows how many humans are in space right now in a Mattermost channel.

![Screenshot](./docs/screenshot.png)

## Requirements

- PHP

## Installation

`composer install`

## Source

https://gitlab.com/pixelbrackets/mattermost-people-in-space

## Demo

🚀 https://mattermost-people-in-space.app.pixelbrackets.de

## License

GNU General Public License version 2 or later

The GNU General Public License can be found at http://www.gnu.org/copyleft/gpl.html.

## Author

Dan Untenzu (<mail@pixelbrackets.de> / [@pixelbrackets](https://pixelbrackets.de))

## Changelog

See [./CHANGELOG.md](CHANGELOG.md)

## Contribution

This script is Open Source, so please use, patch, extend or fork it.
