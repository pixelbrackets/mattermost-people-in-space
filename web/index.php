<?php

require __DIR__ . '/../vendor/autoload.php';

use Pixelbrackets\Html5MiniTemplate\Html5MiniTemplate;

$content = '<h1>🚀 Mattermost People in Space Integration</h1>
<p>
    1. Create a »Slash Command« in your Mattermost instance<br>
    2. Set »###BASEURL###hook/«
       as »Request URL«<br>
    3. Select a command trigger word, for example »humans-in-space«<br>
    4. Type <code>/humans-in-space</code> to trigger the command<br>
</p>';
$content = str_replace(
    '###BASEURL###',
    empty(getenv('BASEURL'))? 'https://example.com/' : getenv('BASEURL'),
    $content
);
$markup = (new Html5MiniTemplate())
    ->setStylesheet('skeleton')
    ->setStylesheetMode(Html5MiniTemplate::STYLE_INLINE)
    ->setContent($content)
    ->getMarkup();
echo $markup;
