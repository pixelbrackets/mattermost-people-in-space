<?php

require __DIR__ . '/../../vendor/autoload.php';

use Pixelbrackets\HumansInSpace\HumansInSpace;

header('Content-type:application/json;charset=utf-8');

// Respond to message
$responseBody = [
    'response_type' => 'in_channel',
    'username' => 'space-bot',
    'text' => '-----' . PHP_EOL . '### :rocket: ' . (new HumansInSpace())->getNumber() . PHP_EOL . '-----',
];

echo json_encode($responseBody);
